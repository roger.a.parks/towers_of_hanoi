import time
import math
from maestro import Controller
from inverse_kinematics import InverseKinematics


class Arm:
    def __init__(self):
        self.L1 = 80
        self.L2 = 80
        self.L3 = 68
        self.ch_base = Channel(pin=0, zero=1490)
        self.ch_elbow = Channel(pin=1, zero=1559)
        self.ch_shoulder = Channel(pin=2, zero=1508)
        self.ch_shoulder.gain = -self.ch_shoulder.gain
        self.ch_grabber = Channel(pin=3, zero=1500)
        self.ik = InverseKinematics(self.L1, self.L2, self.L3)
        self.control = Controller()

        self.x = 0
        self.y = 0
        self.z = 0

    def __enter__(self):
        return self.open()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def open(self):
        self.control.open()
        self.control.set_speed(self.ch_base.pin, 0)
        self.control.set_speed(self.ch_shoulder.pin, 0)
        self.control.set_speed(self.ch_elbow.pin, 0)
        self.control.set_speed(self.ch_grabber.pin, 800)
        self.control.set_acceleration(self.ch_base.pin, 0)
        self.control.set_acceleration(self.ch_shoulder.pin, 0)
        self.control.set_acceleration(self.ch_elbow.pin, 0)
        self.control.set_acceleration(self.ch_grabber.pin, 150)
        self.go_directly_to((0, self.L2 + self.L3, self.L1))
        return self

    def close(self):
        self.control.close()

    def angle_to_pwm(self, servo, angle):
        pwm = 0.5 + servo.zero + servo.gain * angle
        return int(pwm)

    def servos_off(self):
        self.control.set_target(self.ch_base.pin, 0)
        self.control.set_target(self.ch_shoulder.pin, 0)
        self.control.set_target(self.ch_elbow.pin, 0)
        self.control.set_target(self.ch_grabber.pin, 0)

    def go_directly_to(self, x_y_z_list):
        x = x_y_z_list[0]
        y = x_y_z_list[1]
        z = x_y_z_list[2]
        boolean_resp, rad_base, rad_shoulder, rad_elbow = self.ik.solve(x, y, z)

        self.write_base_angle(rad_base)
        self.write_elbow_angle(rad_elbow)
        self.write_shoulder_angle(rad_shoulder)
        self.x = x
        self.y = y
        self.z = z

    def motion_complete(self):
        while self.control.get_moving_state():
            time.sleep(0.002)

    def write_base_angle(self, angle):
        ch = self.ch_base
        pwm = self.angle_to_pwm(ch, angle)
        self.control.set_target(ch.pin, pwm)

    def write_shoulder_angle(self, angle):
        ch = self.ch_shoulder
        pwm = self.angle_to_pwm(ch, angle - (math.pi / 2))
        self.control.set_target(ch.pin, pwm)

    def write_elbow_angle(self, angle):
        ch = self.ch_elbow
        pwm = self.angle_to_pwm(ch, angle)
        self.control.set_target(ch.pin, pwm)

    def grabber_close(self):
        grabber_close = 943
        self.control.set_target(self.ch_grabber.pin, grabber_close)
        self.motion_complete()

    def grabber_open(self):
        grabber_open = 1700
        self.control.set_target(self.ch_grabber.pin, grabber_open)
        self.motion_complete()

    def go_home(self):
        self.goto_point((0, self.L2 + self.L3, self.L1))

    def goto_point(self, x_y_z_list):
        x = x_y_z_list[0]
        y = x_y_z_list[1]
        z = x_y_z_list[2]

        x0 = self.x
        y0 = self.y
        z0 = self.z

        dist = math.sqrt((x0-x)*(x0-x) + (y0-y)*(y0-y) + (z0-z)*(z0-z))

        accel = 0.015
        slew_rate = 30
        step = 0
        accel_dist = (slew_rate * slew_rate) / (2 * accel)
        i = 0
        while i < dist:
            self.go_directly_to((
                x0 + (x-x0)*i/dist,
                y0 + (y-y0)*i/dist,
                z0 + (z-z0)*i/dist,
            ))
            time.sleep(0.001)
            i += step

            dist_traveled = dist - math.sqrt((self.x-x)*(self.x-x) + (self.y-y)*(self.y-y) + (self.z-z)*(self.z-z))

            remaining_distance = dist - dist_traveled
            deccel_dist = (step * step) / (2 * accel)
            if remaining_distance < deccel_dist:
                if step > accel:
                    step = step - accel  # slow down
            elif dist_traveled < accel_dist:
                if step < slew_rate:
                    step = step + accel  # speed up

        self.go_directly_to((x, y, z))
        time.sleep(0.1)


class Channel:
    def __init__(self, pin, zero):
        self.pin = pin
        self.zero = zero
        self.gain = (2000 - 1000) / (math.pi / 2)
