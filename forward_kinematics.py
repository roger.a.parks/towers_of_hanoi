import math


class ForwardKinematics:

    def __init__(self, L1, L2, L3):
        self.L1 = L1
        self.L2 = L2
        self.L3 = L3

    def polar_to_cartesian(self, r: float, theta: float):
        a = r * math.cos(theta)
        b = r * math.sin(theta)
        return a, b

    def unsolve(self, a0: float, a1: float, a2: float):
        u01, v01 = self.polar_to_cartesian(self.L1, a1)
        u12, v12 = self.polar_to_cartesian(self.L2, a2)

        u = u01 + u12 + self.L3
        v = v01 + v12

        y, x = self.polar_to_cartesian(u, a0)
        z = v

        return x, y, z

    def distance(self, x1, y1, z1, x2, y2, z2):
        dx = x2 - x1
        dy = y2 - y1
        dz = z2 - z1

        return dx*dx + dy*dy + dz*dz
