import math


class InverseKinematics:
    def __init__(self, L1, L2, L3):
        self.L1 = L1
        self.L2 = L2
        self.L3 = L3
        pass

    def cartesian_to_polar(self, a, b):
        r = math.sqrt(a * a + b * b)

        if r == 0:
            return

        c = a / r
        s = b / r

        if s > 1:
            s = 1
        if c > 1:
            c = 1
        if s < -1:
            s = -1
        if c < -1:
            c = -1

        theta = math.acos(c)

        if s < 0:
            theta *= -1

        return r, theta

    def cosangle(self, opp, adj1, adj2):
        den = 2 * adj1 * adj2

        if den == 0:
            return False, 0

        c = (adj1 * adj1 + adj2 * adj2 - opp * opp) / den

        if c > 1 or c < -1:
            return False, 0

        theta = math.acos(c)

        return True, theta

    def solve(self, x, y, z):
        r, th0 = self.cartesian_to_polar(y, x)

        r -= self.L3

        R, ang_P = self.cartesian_to_polar(r, z)

        bool_resp, B = self.cosangle(self.L2, self.L1, R)
        if not bool_resp:
            return False

        bool_resp, C = self.cosangle(R, self.L1, self.L2)
        if not bool_resp:
            return False

        a0 = th0
        a1 = ang_P + B
        a2 = C + a1 - math.pi

        return True, a0, a1, a2
