import serial
from serial.tools.list_ports import grep
"""
---------------------------
 Maestro Servo Controller
---------------------------

Support for the Pololu Maestro line of servo controllers

Steven Jacobs -- Aug 2013
https://github.com/FRC4564/Maestro/

These functions provide access to many of the Maestro's capabilities using the
Pololu serial protocol
"""


class Controller:
    # When connected via USB, the Maestro creates two virtual serial ports
    # /dev/ttyACM0 for commands and /dev/ttyACM1 for communications.
    # Be sure the Maestro is configured for "USB Dual Port" serial mode.
    # "USB Chained Mode" may work as well, but hasn't been tested.
    #
    # Pololu protocol allows for multiple Maestros to be connected to a single
    # serial port. Each connected device is then indexed by number.
    # This device number defaults to 0x0C (or 12 in decimal), which this module
    # assumes. If two or more controllers are connected to different serial
    # ports, or you are using a Windows OS, you can provide the tty port. For
    # example, '/dev/ttyACM2' or for Windows, something like 'COM3'.
    def __init__(self, device=0x0c):
        self.device = device
        self.usb = serial.Serial()

        # Command lead-in and device number are sent for each Pololu serial command.
        self.pololu_cmd = bytes([0xaa, self.device])

    def open(self):
        # Open the command port
        self.usb = serial.Serial(
            port=self.find_port(),
            timeout=1,
            baudrate=9600,
        )

    # Cleanup by closing USB serial port
    def close(self):
        self.usb.close()

    def find_port(self):
        pololu_ports = grep("Pololu")
        first_com = next(pololu_ports)
        second_com = next(pololu_ports)

        # get first com port
        if first_com.device < second_com.device:
            pololu_device_name = first_com.device
        else:
            pololu_device_name = second_com.device

        return pololu_device_name

    # Send a Pololu command out the serial port
    def send_command(self, cmd):
        cmd_str = self.pololu_cmd + cmd.encode()
        self.usb.write(cmd_str)

    def target_to_bytes(self, target):
        target_quarter_micro = target * 4
        lsb = target_quarter_micro & 0x7f  # 7 bits for least significant byte
        msb = (target_quarter_micro >> 7) & 0x7f  # shift 7 and take next 7 bits for msb
        return chr(lsb) + chr(msb)

    def set_target(self, channel, target):
        cmd = chr(0x04) + chr(channel) + self.target_to_bytes(target)
        self.send_command(cmd)

    # Set speed of channel
    # Speed is measured as 0.25microseconds/10milliseconds
    # For the standard 1ms pulse width change to move a servo between extremes, a speed
    # of 1 will take 1 minute, and a speed of 60 would take 1 second.
    # Speed of 0 is unrestricted.
    def set_speed(self, channel, speed):
        lsb = speed & 0x7f  # 7 bits for least significant byte
        msb = (speed >> 7) & 0x7f  # shift 7 and take next 7 bits for msb
        cmd = chr(0x07) + chr(channel) + chr(lsb) + chr(msb)
        self.send_command(cmd)

    # Set acceleration of channel
    # This provide soft starts and finishes when servo moves to target position.
    # Valid values are from 0 to 255. 0=unrestricted, 1 is slowest start.
    # A value of 1 will take the servo about 3s to move between 1ms to 2ms range.
    def set_acceleration(self, channel, accel):
        lsb = accel & 0x7f  # 7 bits for least significant byte
        msb = (accel >> 7) & 0x7f  # shift 7 and take next 7 bits for msb
        cmd = chr(0x09) + chr(channel) + chr(lsb) + chr(msb)
        self.send_command(cmd)
    
    # Get the current position of the device on the specified channel
    # The result is returned in a measure of quarter-microseconds, which mirrors
    # the Target parameter of setTarget.
    # This is not reading the true servo position, but the last target position sent
    # to the servo. If the Speed is set to below the top speed of the servo, then
    # the position result will align well with the actual servo position, assuming
    # it is not stalled or slowed.
    def get_position(self, channel):
        cmd = chr(0x10) + chr(channel)
        self.send_command(cmd)
        lsb = ord(self.usb.read())
        msb = ord(self.usb.read())
        position_quarter_micro = (msb << 8) + lsb
        return position_quarter_micro / 4

    # Have all servo outputs reached their targets? This is useful only if Speed and/or
    # Acceleration have been set on one or more of the channels. Returns True or False.
    # Not available with Micro Maestro.
    def get_moving_state(self):
        cmd = chr(0x13)
        self.send_command(cmd)
        is_moving = self.usb.read() == bytes([0x01])
        return is_moving

    def get_errors(self):
        cmd = chr(0x21)
        self.send_command(cmd)
        return self.usb.read(2)

    def go_home(self):
        cmd = chr(0x22)
        self.send_command(cmd)

    def multiple_targets(self, start_channel, targets):
        target_commands = ""
        for target in targets:
            target_commands += self.target_to_bytes(target)
        cmd = chr(0x1f) + chr(len(targets)) + chr(start_channel) + target_commands
        self.send_command(cmd)

    def run_script_subroutine(self, subroutine_number):
        cmd = chr(0x27) + chr(subroutine_number)
        # can pass a param with command 0x28
        # cmd = chr(0x28) + chr(subNumber) + chr(lsb) + chr(msb)
        self.send_command(cmd)

    def stop_script_subroutine(self):
        cmd = chr(0x24)
        self.send_command(cmd)
