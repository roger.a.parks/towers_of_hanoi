import time

from gpiozero import OutputDevice, Button

from arm import Arm

servos_enabled_pin = OutputDevice(18)
button_1_pin = Button(14)
button_2_pin = Button(15)

# define positions
height_above = 30
height_top = -8
height_middle = -23
height_bottom = -38

right = (-95, 173)
middle = (-6, 179)
left = (92, 174)

above_middle = middle + (height_above,)
top_middle = middle + (height_top,)
middle_middle = middle + (height_middle,)
bottom_middle = middle + (height_bottom,)

above_left = left + (height_above,)
top_left = left + (height_top,)
middle_left = left + (height_middle,)
bottom_left = left + (height_bottom,)

above_right = right + (height_above,)
top_right = right + (height_top,)
middle_right = right + (height_middle,)
bottom_right = right + (height_bottom,)


def seq_0(arm):
    arm.goto_point(above_middle)
    arm.goto_point(top_middle)
    arm.grabber_close()
    arm.goto_point(above_middle)

    arm.goto_point(above_left)
    arm.goto_point(bottom_left)
    arm.grabber_open()
    arm.goto_point(above_left)

    arm.goto_point(above_middle)
    arm.goto_point(middle_middle)
    arm.grabber_close()
    arm.goto_point(above_middle)

    arm.goto_point(above_left)
    arm.goto_point(middle_left)
    arm.grabber_open()
    arm.goto_point(above_left)

    arm.goto_point(above_middle)
    arm.goto_point(bottom_middle)
    arm.grabber_close()
    arm.goto_point(above_middle)

    arm.goto_point(above_left)
    arm.goto_point(top_left)
    arm.grabber_open()
    arm.goto_point(above_left)

    # and back
    arm.goto_point(above_left)
    arm.goto_point(top_left)
    arm.grabber_close()
    arm.goto_point(above_left)

    arm.goto_point(above_middle)
    arm.goto_point(bottom_middle)
    arm.grabber_open()
    arm.goto_point(above_middle)

    arm.goto_point(above_left)
    arm.goto_point(middle_left)
    arm.grabber_close()
    arm.goto_point(above_left)

    arm.goto_point(above_middle)
    arm.goto_point(middle_middle)
    arm.grabber_open()
    arm.goto_point(above_middle)

    arm.goto_point(above_left)
    arm.goto_point(bottom_left)
    arm.grabber_close()
    arm.goto_point(above_left)

    arm.goto_point(above_middle)
    arm.goto_point(top_middle)
    arm.grabber_open()
    arm.goto_point(above_middle)


def seq_1(arm):
    arm.goto_point(above_middle)
    arm.goto_point(top_middle)
    arm.grabber_close()
    arm.goto_point(above_middle)

    arm.goto_point(above_right)
    arm.goto_point(bottom_right)
    arm.grabber_open()
    arm.goto_point(above_right)

    arm.goto_point(above_middle)
    arm.goto_point(middle_middle)
    arm.grabber_close()
    arm.goto_point(above_middle)

    arm.goto_point(above_right)
    arm.goto_point(middle_right)
    arm.grabber_open()
    arm.goto_point(above_right)

    arm.goto_point(above_middle)
    arm.goto_point(bottom_middle)
    arm.grabber_close()
    arm.goto_point(above_middle)

    arm.goto_point(above_right)
    arm.goto_point(top_right)
    arm.grabber_open()
    arm.goto_point(above_right)

    # and back
    arm.goto_point(above_right)
    arm.goto_point(top_right)
    arm.grabber_close()
    arm.goto_point(above_right)

    arm.goto_point(above_middle)
    arm.goto_point(bottom_middle)
    arm.grabber_open()
    arm.goto_point(above_middle)

    arm.goto_point(above_right)
    arm.goto_point(middle_right)
    arm.grabber_close()
    arm.goto_point(above_right)

    arm.goto_point(above_middle)
    arm.goto_point(middle_middle)
    arm.grabber_open()
    arm.goto_point(above_middle)

    arm.goto_point(above_right)
    arm.goto_point(bottom_right)
    arm.grabber_close()
    arm.goto_point(above_right)

    arm.goto_point(above_middle)
    arm.goto_point(top_middle)
    arm.grabber_open()
    arm.goto_point(above_middle)


def seq_2(arm):
    arm.grabber_open()
    arm.goto_point(above_middle)
    arm.goto_point(top_middle)
    arm.grabber_close()
    arm.goto_point(above_middle)

    arm.goto_point(above_left)
    arm.goto_point(bottom_left)
    arm.goto_point(above_left)

    arm.goto_point(above_right)
    arm.goto_point(bottom_right)
    arm.goto_point(above_right)

    arm.goto_point(above_middle)
    arm.goto_point(top_middle)
    arm.grabber_open()
    arm.goto_point(above_middle)


def seq_3(arm):
    arm.grabber_open()
    arm.goto_point(above_middle)
    arm.goto_point(above_left)
    arm.goto_point(above_right)
    arm.goto_point(above_middle)


def get_pen(arm):
    above_pen = (105, 70, 83)
    at_pen = (98, 71, 43)

    arm.grabber_open()
    time.sleep(0.1)
    arm.goto_point(above_pen)
    arm.goto_point(at_pen)
    arm.grabber_close()
    arm.goto_point(above_pen)
    for wiggle_direction in [1, -1]:  # wiggle to free pen
        for axis in [0, 1]:
            wiggle_target = list(above_pen)
            wiggle_target[axis] += (10 * wiggle_direction)
            arm.goto_point(wiggle_target)
    arm.goto_point(above_pen)

    above_drop_point = (50, 160, 100)
    at_drop_point = (50, 160, 40)

    arm.goto_point(above_drop_point)
    arm.goto_point(at_drop_point)
    time.sleep(0.8)
    arm.grabber_open()
    arm.goto_point(above_drop_point)
    arm.go_home()


if __name__ == '__main__':
    servos_enabled_pin.off()
    with Arm() as global_arm:
        while True:
            button_1_was_pressed = button_1_pin.is_pressed
            button_2_was_pressed = button_2_pin.is_pressed

            if button_1_was_pressed or button_2_was_pressed:
                servos_enabled_pin.on()
                time.sleep(0.5)
                global_arm.go_home()
                global_arm.grabber_open()

                if button_1_was_pressed:
                    get_pen(global_arm)
                elif button_2_was_pressed:
                    seq_0(global_arm)
                    seq_1(global_arm)
                    seq_2(global_arm)
                    seq_3(global_arm)

                global_arm.grabber_open()
                global_arm.go_home()
                time.sleep(0.5)
                servos_enabled_pin.off()
